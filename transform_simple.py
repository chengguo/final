from transforms import rotate_right, mirror, grayscale, blur
import sys
from images import read_img, write_img


def main():
    image_file = sys.argv[1]
    transform = sys.argv[2]
    img = read_img(image_file)

    if transform == 'grayscale':
        img = grayscale(img)
    elif transform == 'mirror':
        img = mirror(img)
    elif transform == 'blur':
        img = blur(img)
    elif transform == 'rotate_right':
        img = rotate_right(img)

    base_filename = image_file.split('.')[0]
    ext = image_file.split('.')[1]
    out_file = base_filename + "_trans." + ext

    write_img(img, out_file)


if __name__ == '__main__':
    main()
