def change_colors(image: list[list[tuple[int, int, int]]],
                  to_change: tuple[int, int, int],
                  to_change_to: tuple[int, int, int]) -> list[list[tuple[int, int, int]]]:
    for i in range(len(image)):
        for j in range(len(image[0])):
            if image[i][j] == to_change:
                image[i][j] = to_change_to

    return image


def rotate_right(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    rotated = []
    num_rows = len(image)
    num_cols = len(image[0])

    for j in range(num_cols):
        rotated.append([])
        for i in range(num_rows - 1, -1, -1):
            rotated[-1].append(image[i][j])

    return rotated


def mirror(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    num_rows = len(image)
    num_cols = len(image[0])

    mirrored_image = [[(0, 0, 0) for _ in range(num_cols)] for _ in range(num_rows)]

    for i in range(num_cols):
        mirrored_image[num_cols - i - 1] = image[i]

    return mirrored_image


def rotate_colors(image: list[list[tuple[int, int, int]]], increment: int) -> list[list[tuple[int, int, int]]]:
    new_image = []
    for row in image:
        new_row = []
        for pixel in row:
            new_pixel = [(value + increment) % 256 for value in pixel]
            new_row.append(tuple(new_pixel))
        new_image.append(new_row)
    return new_image


def blur(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    new_image = []
    for i in range(len(image)):
        new_row = []
        for j in range(len(image[0])):
            avg = [0, 0, 0]
            for dx in [-1, 0, 1]:
                for dy in [-1, 0, 1]:
                    if i + dy >= 0 and i + dy < len(image) and j + dx >= 0 and j + dx < len(image[0]):
                        for k in range(3):
                            avg[k] += image[i + dy][j + dx][k]
            for k in range(3):
                avg[k] = int(avg[k] / 9)
            new_row.append(tuple(avg))
        new_image.append(new_row)
    return new_image


def shift(image: list[list[tuple[int, int, int]]], horizontal: int, vertical: int) -> list[list[tuple[int, int, int]]]:
    new_image = []
    for i in range(len(image)):
        new_row = [None] * len(image[0])
        for j in range(len(image[0])):
            new_i = i + vertical
            new_j = j + horizontal
            if 0 <= new_i < len(image) and 0 <= new_j < len(image[0]):
                new_row[j] = image[new_i][new_j]
        new_image.append(new_row)
    return new_image


def crop(image: list[list[tuple[int, int, int]]], x: int, y: int, width: int, height: int) -> list[
    list[tuple[int, int, int]]]:
    new_image = []
    for i in range(y, y + height):
        new_row = []
        for j in range(x, x + width):
            new_row.append(image[i][j])
        new_image.append(new_row)
    return new_image


def grayscale(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    new_image = []
    for row in image:
        new_row = []
        for pixel in row:
            avg = sum(pixel) // 3
            new_row.append((avg, avg, avg))
        new_image.append(new_row)
    return new_image


def filter(image: list[list[tuple[int, int, int]]], r: float, g: float, b: float) -> list[list[tuple[int, int, int]]]:
    new_image = []
    for row in image:
        new_row = []
        for pixel in row:
            new_pixel = (min(int(pixel[0] * r), 255),
                         min(int(pixel[1] * g), 255),
                         min(int(pixel[2] * b), 255))
            new_row.append(new_pixel)
        new_image.append(new_row)
    return new_image
