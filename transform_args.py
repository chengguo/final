from transforms import change_colors, rotate_colors, shift
import sys
from images import read_img, write_img


def main():
    image_file = sys.argv[1]
    transform = sys.argv[2]
    img = read_img(image_file)

    if transform == 'change_colors':
        return change_colors(img, *sys.argv[3:])

    elif transform == 'rotate_colors':
        return rotate_colors(img, int(sys.argv[3]))

    elif transform == 'shift':
        return shift(img, int(sys.argv[3]), int(sys.argv[4]))

    base_filename = sys.argv[1].split('.')[0]
    ext = sys.argv[1].split('.')[1]
    out_file = base_filename + "_trans." + ext
    write_img(img, out_file)


if __name__ == '__main__':
    main()
