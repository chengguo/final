import sys
from transforms import rotate_colors, mirror, blur, change_colors, shift
from images import read_img, write_img


def main():
    image_file = sys.argv[1]
    img = read_img(image_file)

    i = 2
    while i < len(sys.argv):
        transform = sys.argv[i]
        if transform == 'rotate_colors':
            n = int(sys.argv[i + 1])
            img = rotate_colors(img, n)
            i += 2
        elif transform == 'mirror':
            img = mirror(img)
            i += 1
        elif transform == 'blur':
            img = blur(img)
            i += 1
        elif transform == 'change_colors':
            r1, g1, b1, r2, g2, b2 = [int(x) for x in sys.argv[i + 1:i + 7]]
            img = change_colors(img, r1, g1, b1, r2, g2, b2)
            i += 7
        elif transform == 'shift':
            dx, dy = [int(x) for x in sys.argv[i + 1:i + 3]]
            img = shift(img, dx, dy)
            i += 3

    base_filename = sys.argv[1].split('.')[0]
    ext = sys.argv[1].split('.')[1]
    out_file = base_filename + "_trans." + ext
    write_img(img, out_file)


if __name__ == '__main__':
    main()
